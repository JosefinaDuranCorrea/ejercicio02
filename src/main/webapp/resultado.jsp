<%-- 
    Document   : resultado
    Created on : 8/04/2021, 06:10:22 PM
    Author     : Josefina
--%>

<%@page import="cl.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<% 
    Calculadora calculadora=(Calculadora)request.getAttribute("calculadora");
    
%> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    El resultado es: <%= calculadora.getResultadoSuma()%>
    </body>
</html>
